package org.coffin.spatialindex.gindex;

import org.coffin.spatialindex.Envelope;
import org.locationtech.jts.index.strtree.STRtree;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author eric.coffin@unb.ca
 * GIndex or Global Index.
 * Maintains and in-memory spatial index of RIndex envelopes pointing to RIndexURLs.
 */
public class GIndex {

    File file;
    STRtree rTree;


    public GIndex() {
        rTree = new STRtree();
    }


    public void insert(org.coffin.spatialindex.Envelope envelope, String region) {
        org.locationtech.jts.geom.Envelope _envelope = new org.locationtech.jts.geom.Envelope(
                envelope.getMinX(), envelope.getMaxX(), envelope.getMinY(), envelope.getMaxY());
        rTree.insert(_envelope, region);
    }

    public List<String> lookup(Envelope envelope) {
        org.locationtech.jts.geom.Envelope _envelope = new org.locationtech.jts.geom.Envelope(
                envelope.getMinX(), envelope.getMaxX(), envelope.getMinY(), envelope.getMaxY());

        List<String> response = new ArrayList<>();

        List q = rTree.query(_envelope);
        if (q != null) {
            for (Object obj : q) {
                response.add((String) obj);
            }
        }
        return response;
    }
}